//
//  ViewController.swift
//  SkyMobileExercise
//
//  Created by Gustavo Gomes de Oliveira on 15/01/18.
//  Copyright © 2018 Gustavo Gomes de Oliveira. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    var delegate: AppDelegate?
    var moviesImage: [UIImage] = []
    var imageURL: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
               
        delegate = UIApplication.shared.delegate as? AppDelegate
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.delegate?.movies.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movie_cell", for: indexPath) as! MovieCollectionViewCell
        
        if ((self.delegate?.movies.count)! > 0)
        {
        
            cell.movieTittle.text = self.delegate?.movies[indexPath.row].title
            cell.movieImage.image = self.delegate?.movies[indexPath.row].moviePoster
        }
        
        return cell
        
    }

}

