//
//  InitialScreenViewController.swift
//  SkyMobileExercise
//
//  Created by Gustavo Gomes de Oliveira on 16/01/18.
//  Copyright © 2018 Gustavo Gomes de Oliveira. All rights reserved.
//

import UIKit

class InitialScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "segue_movie_list"){
            
            let backItem = UIBarButtonItem()
            backItem.title = "Cine SKY"
            backItem.tintColor = UIColor.white
            navigationItem.backBarButtonItem = backItem
            
        }
        
    }

    @IBAction func movieList(_ sender: Any) {
        
        performSegue(withIdentifier: "segue_movie_list", sender: self)
    }
    
    
}
