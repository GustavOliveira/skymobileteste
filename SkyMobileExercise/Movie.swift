//
//  Movie.swift
//  SkyMobileExercise
//
//  Created by Gustavo Gomes de Oliveira on 16/01/18.
//  Copyright © 2018 Gustavo Gomes de Oliveira. All rights reserved.
//

import UIKit

class Movie: NSObject {

    var title: String?
    var moviePoster: UIImage?
    var posterURL: String?
    
}
