//
//  MovieCollectionViewCell.swift
//  SkyMobileExercise
//
//  Created by Gustavo Gomes de Oliveira on 15/01/18.
//  Copyright © 2018 Gustavo Gomes de Oliveira. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var movieTittle: UILabel!
    
}
